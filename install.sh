#!/data/data/com.termux/files/usr/bin/bash

# echo with color
echolor() {
	w='\033[0m'
	# default
	if [ $1 == 'd' ]; then
		a='\033[32m'
		b='\033[36m'
		c='+'
	# error
	elif [ $1 == 'e' ]; then
		a='\033[31m'
		b='\033[33m'
		c='!'
	fi

	echo -en "$w[$a$c$w] $b$2$w"
}

SYSBIN=/system/bin
SYSXBIN=/system/xbin
PRE=/data/data/com.termux/files
ROOT_HOME=$PRE/home/.suroot
LDLP="export LD_LIBRARY_PATH=$PRE/usr/lib"

# finding su executable
echolor 'd' 'su executable path: '
for p in /sbin /system/sbin /system/bin /system/xbin /su/bin /magisk/.core/bin; do
	if [ -x $p/su ]; then
		SU=$p/su
		echo $p
	fi
done

if [ -z $SU ]; then
	echo -e "\nnot found"
	echolor 'e' "'sudo' requires 'su'\n"
	exit
fi

# head for sudo file
cat > head << EOM
#!/data/data/com.termux/files/usr/bin/bash
SU=$SU

EOM

# install required package (maybe)
echolor 'd' "package update...\n"
apt update && apt upgrade -y 
echolor 'd' "install package root-repo, ncurses-utils, tsu\n"
apt install root-repo ncurses-utils tsu -y

# create root folder
if [ -x "/sbin/magisk" ]; then
	unset LD_LIBRARY_PATH
	CMDLINE="PATH=$PATH:$SYSXBIN:$SYSBIN;$LDLP;HOME=$ROOT_HOME;cd $PWD"
fi

echolor 'd' "mkdir $ROOT_HOME\n"
$SU -c "`$CMDLINE`mkdir $ROOT_HOME"
echolor 'd' "create $ROOT_HOME/.bashrc\n"
$SU -c "`$CMDLINE`cat > $ROOT_HOME/.bashrc << EOM
PS1='\[\e[0;32m\]\w\[\e[0m\] \[\e[0;97m\]#\[\e[0m\] '
export TERM=$TERM
$LDLP
export PATH=$PATH:$SYSXBIN:$SYSBIN
EOM"
$SU -c "`$CMDLINE`chmod -R 700 $ROOT_HOME"

# install sudo
echolor 'd' "then install sudo.. "
cat head sudo > $PRE/usr/bin/sudo
chmod 700 $PRE/usr/bin/sudo
rm head
echo 'done'